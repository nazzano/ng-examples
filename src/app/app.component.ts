import { Component, Input } from '@angular/core'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // title è il sito nel momento in cui io assegno una azione
  // posso anche dire il tipo del sito,ad esempio stringa che sarebbe il datatype
  public title : string = 'etl-ng';

    // constructor() {
    //   setTimeout(() => this.title = 'cambiato', 5000);
    // }

  public handleClick(): void {
    alert('Hai fatto click');
  }

  @Input() public prova: number = 5;

  constructor() {
    setTimeout(() => this.prova = 10, 5000);
  }

  public handleLimitReached (payload: number): void {
    alert(payload);
  }
}


