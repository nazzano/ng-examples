import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { ArenaComponent } from './components/arena/arena.component';
import { HeroComponent } from './components/hero/hero.component';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UsersTableRowComponent } from './components/users-table-row/users-table-row.component';
import { HighlightDirective } from './directives/highlight.directive';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { ForbiddenDirective } from './directives/forbidden.directive';

@NgModule({
  declarations: [
    AppComponent,
    ArenaComponent,
    HeroComponent,
    UsersTableComponent,
    UsersTableRowComponent,
    HighlightDirective,
    UserComponent,
    LoginComponent,
    ForbiddenDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    // UserService,
    // Equivalente di UserService:
    // {
    //   provide: UserService, // Token
    //   useClass: UserService // Dipendenza
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
