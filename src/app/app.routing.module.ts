import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArenaComponent } from './components/arena/arena.component';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [{
  component: ArenaComponent,
  path: 'arena'
}, {
  component: LoginComponent,
  path: 'login'
}, {
  component: UserComponent,
  path: 'users/:id'
}, {
  component: UsersTableComponent,
  path: 'users'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
