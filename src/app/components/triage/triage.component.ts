import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-triage',
  templateUrl: './triage.component.html',
  styleUrls: ['./triage.component.scss']
})
export class TriageComponent implements OnInit {

  // color è il parametro formale che diventerà attuale quando richiamo il metodo
  public color: string = "";

  public buttonTriage(but: string): void {
    this.color = but;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
