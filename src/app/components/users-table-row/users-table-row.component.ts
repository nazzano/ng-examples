import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: '[app-users-table-row]',
  templateUrl: './users-table-row.component.html',
  styleUrls: ['./users-table-row.component.scss']
})
export class UsersTableRowComponent implements OnChanges, OnInit {

  @Input() public user!: User;

  @Output() public delete: EventEmitter<User> = new EventEmitter();

  constructor() {
  }

  public handleDeleteClick(): void {
    this.delete.emit(this.user);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes['user']);
  }

  ngOnInit(): void {
  }

}
