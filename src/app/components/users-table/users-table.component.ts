import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnDestroy, OnInit {
 
  private _users$!: Subscription;

  public users: User[] = [];

  constructor(private _userService: UserService) {
  }

  public handleDelete(user: User): void {
    this.users.splice(
      this.users.indexOf(user),
      1
    );
  }

  public ngOnDestroy(): void {
      this._users$.unsubscribe();
  }

  public ngOnInit(): void {
    this._users$ = this._userService.list().subscribe(users => this.users = users);
  }

}
