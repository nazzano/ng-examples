export interface IUser {
    email: string;
    id: number;
    name: string;
    phonr: string;
    username: string;
    website: string;

}