import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// Interfaces
import { IUser } from '../interfaces.ts/user.interface';
// Models
import { User } from '../models/user';

// Rxjs
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

// L'oggetto sul decorator @Injectable
// esiste da Angular 6 e serve ad applicare i
// meccanismi di Tree Shaking sui servizi
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _objToModel(obj: IUser): User {
    return new User(obj.id, obj.name, obj.email);
  }

  constructor(private _http: HttpClient) {
    console.log('user service creato!!');
  }

  public get(id: number) {
    return this._http.get<IUser>(`${environment.api.url}/users/${id}`).pipe(
      map(user => this._objToModel(user))
    );
  }

  public list(): Observable<User[]> {
    return this._http.get<IUser[]>(`${environment.api.url}/users`).pipe(
      map(users => users.map(user => new User(user.id, user.email, user.name)))
    );
  }
}