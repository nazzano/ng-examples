import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

// fn esterna 
export const forbiddenValidator = (...values: string[]): ValidatorFn => {
    // fn interna, che viene restituita da quella esterna, verrà usata da Angular per eseguire la validazione sul campo in input / group
    //
    // Quando viene invocato da Angular, le viene passato Control istanziato sull'elemento del Form
    return (c: AbstractControl): ValidationErrors | null => {
        for (const value of values) {
            // Se quando digitato dall'utente (c.value) coicnide con uno degli elemtni nell'array restituisco un errore
            if (value === c.value) {
                return {
                    forbidden: true
                };
            }
        }
        // null perche se non esiste un oggetto non esiste neanche l'istanza
        // Nel caso in cui non si devùbba restituire un errore 
        return null;
    };
};